*** Darwin 9x10 ***
Turn = 0.
  0123456789
0 ..........
1 ..........
2 ..........
3 ..........
4 ..........
5 ..........
6 r.........
7 ..........
8 ..........

*** Darwin 7x10 ***
Turn = 0.
  0123456789
0 ..ffh.t...
1 ......r...
2 ..fr.t....
3 ...h......
4 .....fhh.f
5 ...f....t.
6 ..........

*** Darwin 3x5 ***
Turn = 0.
  01234
0 .r...
1 .....
2 ...t.

*** Darwin 9x5 ***
Turn = 0.
  01234
0 ...h.
1 .....
2 ..r..
3 ff.h.
4 .....
5 .ff..
6 .tth.
7 ...t.
8 ...h.

*** Darwin 5x5 ***
Turn = 0.
  01234
0 .....
1 .....
2 .....
3 .....
4 ..t..

*** Darwin 2x6 ***
Turn = 0.
  012345
0 f..hh.
1 fh.f..

Turn = 6.
  012345
0 f..h..
1 f.hfh.

Turn = 12.
  012345
0 f..h..
1 f.hfh.

*** Darwin 4x8 ***
Turn = 0.
  01234567
0 ..f.....
1 .h.....f
2 ........
3 .rh...h.

*** Darwin 7x5 ***
Turn = 0.
  01234
0 .....
1 ...hh
2 f...f
3 .hh..
4 ....h
5 .t...
6 f....

*** Darwin 4x10 ***
Turn = 0.
  0123456789
0 .r......f.
1 ..r.t.t..f
2 ...t.f.tr.
3 .....f....

Turn = 6.
  0123456789
0 ........f.
1 ....t.t..f
2 ..tt.f.t..
3 ..t..f.r..

Turn = 12.
  0123456789
0 ........f.
1 ....t.t..f
2 ..tt.f.t..
3 ..t..f.t..

*** Darwin 1x10 ***
Turn = 0.
  0123456789
0 .tffth.rtt

Turn = 14.
  0123456789
0 .ttttt.ttt

*** Darwin 4x4 ***
Turn = 0.
  0123
0 ....
1 .f..
2 ....
3 ....

*** Darwin 10x1 ***
Turn = 0.
  0
0 .
1 .
2 .
3 r
4 .
5 .
6 .
7 .
8 r
9 f

*** Darwin 10x4 ***
Turn = 0.
  0123
0 .f..
1 ....
2 ....
3 ....
4 ....
5 ....
6 ....
7 ....
8 .r..
9 ....

Turn = 2.
  0123
0 .f..
1 ....
2 ....
3 ....
4 ....
5 ....
6 ....
7 ....
8 r...
9 ....

Turn = 4.
  0123
0 .f..
1 ....
2 ....
3 ....
4 ....
5 ....
6 r...
7 ....
8 ....
9 ....

Turn = 6.
  0123
0 .f..
1 ....
2 ....
3 ....
4 r...
5 ....
6 ....
7 ....
8 ....
9 ....

Turn = 8.
  0123
0 .f..
1 ....
2 r...
3 ....
4 ....
5 ....
6 ....
7 ....
8 ....
9 ....

Turn = 10.
  0123
0 rf..
1 ....
2 ....
3 ....
4 ....
5 ....
6 ....
7 ....
8 ....
9 ....

Turn = 12.
  0123
0 rf..
1 ....
2 ....
3 ....
4 ....
5 ....
6 ....
7 ....
8 ....
9 ....

Turn = 14.
  0123
0 r...
1 .r..
2 ....
3 ....
4 ....
5 ....
6 ....
7 ....
8 ....
9 ....

Turn = 16.
  0123
0 ..r.
1 ....
2 ....
3 .r..
4 ....
5 ....
6 ....
7 ....
8 ....
9 ....

Turn = 18.
  0123
0 ...r
1 ....
2 ....
3 ....
4 ....
5 .r..
6 ....
7 ....
8 ....
9 ....

*** Darwin 7x5 ***
Turn = 0.
  01234
0 .h...
1 .....
2 .....
3 .f...
4 ...h.
5 .h...
6 ....t

Turn = 12.
  01234
0 h....
1 .....
2 .....
3 .f...
4 .h...
5 .....
6 ...tt

*** Darwin 7x10 ***
Turn = 0.
  0123456789
0 ..........
1 ........r.
2 ..........
3 ....h.....
4 ..........
5 ..........
6 ..........

Turn = 13.
  0123456789
0 ....h.....
1 ..........
2 ..........
3 ..........
4 ..........
5 ..........
6 .r........

*** Darwin 5x1 ***
Turn = 0.
  0
0 h
1 .
2 r
3 h
4 t

*** Darwin 9x5 ***
Turn = 0.
  01234
0 ...f.
1 ..r..
2 .....
3 tf...
4 .....
5 ...h.
6 t...f
7 .t.t.
8 .....

Turn = 5.
  01234
0 r..f.
1 .....
2 .....
3 tt...
4 .....
5 t....
6 t...f
7 .t.t.
8 .....

Turn = 10.
  01234
0 ..rr.
1 .....
2 .....
3 tt...
4 .....
5 t....
6 t...f
7 .t.t.
8 .....

*** Darwin 4x7 ***
Turn = 0.
  0123456
0 ...fr.h
1 tt...rf
2 .t....f
3 ....r.r

Turn = 8.
  0123456
0 ...rr.r
1 tt.....
2 .rt...f
3 .tt..r.

*** Darwin 9x7 ***
Turn = 0.
  0123456
0 .....r.
1 ......t
2 .......
3 .......
4 ......t
5 .t...t.
6 rt.....
7 .......
8 .......

Turn = 11.
  0123456
0 r.r....
1 r......
2 .......
3 .......
4 ......t
5 .t...t.
6 .t.....
7 .......
8 .......

*** Darwin 10x9 ***
Turn = 0.
  012345678
0 ....r....
1 .........
2 ..h......
3 .....t...
4 .........
5 .........
6 .......r.
7 .........
8 ......r..
9 .......t.

Turn = 2.
  012345678
0 ......r..
1 .........
2 h........
3 .....t...
4 .........
5 .........
6 ........r
7 .........
8 ....r....
9 .......t.

Turn = 4.
  012345678
0 ........r
1 .........
2 h........
3 .....t...
4 .........
5 .........
6 .........
7 .........
8 ..r.....r
9 .......t.

Turn = 6.
  012345678
0 ........r
1 .........
2 h........
3 .....t...
4 .........
5 .........
6 .........
7 .........
8 r........
9 .......tt

Turn = 8.
  012345678
0 .........
1 ........r
2 h........
3 .....t...
4 .........
5 .........
6 .........
7 r........
8 .........
9 .......tt

Turn = 10.
  012345678
0 .........
1 .........
2 h........
3 .....t..r
4 .........
5 r........
6 .........
7 .........
8 .........
9 .......tt

Turn = 12.
  012345678
0 .........
1 .........
2 h........
3 r....t...
4 .........
5 ........r
6 .........
7 .........
8 .........
9 .......tt

Turn = 14.
  012345678
0 .........
1 .........
2 r........
3 r....t...
4 .........
5 .........
6 .........
7 ........r
8 .........
9 .......tt

Turn = 16.
  012345678
0 r........
1 .........
2 .........
3 .....t...
4 r........
5 .........
6 .........
7 .........
8 ........r
9 .......tr

Turn = 18.
  012345678
0 .r.......
1 .........
2 .........
3 .....t...
4 .........
5 .........
6 r........
7 .........
8 ........r
9 .......tr

Turn = 20.
  012345678
0 ...r.....
1 .........
2 .........
3 .....t...
4 .........
5 .........
6 ........r
7 .........
8 r........
9 .......tt

Turn = 22.
  012345678
0 .....r...
1 .........
2 .........
3 .....t...
4 ........r
5 .........
6 .........
7 .........
8 .........
9 r......tt
